
# Testing with [Artillery](https://artillery.io)

## Install
```
npm install -g artillery
# check
artillery dino
artillery --version
```

## Run a quick test
```
artillery quick --duration 10 --rate 10 -n 20 http://localhost:3000
```

## Reading the Output
- Sample output
```
  Scenarios launched:  100
  Scenarios completed: 100
  Requests completed:  100
  RPS sent: 9.62
  Request latency:
    min: 4.5
    max: 36.6
    median: 8.1
    p95: 24.1
    p99: 33.1
  Scenario duration:
    min: 5.5
    max: 45.4
    median: 10
    p95: 33.7
    p99: 43
  Scenario counts:
    0: 100 (100%)
  Codes:
    200: 100
```

- Scenarios launched is the number of virtual users created in the preceding 10 seconds (or in total for the final report)

- Scenarios completed is the number of virtual users that completed their scenarios in the preceding 10 seconds (or in the whole test). Note: this is the number of completed sessions, not the number of sessions started and completed in a 10 second interval.

- Requests completed is the number of HTTP requests and responses or WebSocket messages sent

- RPS sent is the average number of requests per second completed in the preceding 10 seconds (or throughout the test)

- Request latency and Scenario duration are in milliseconds, and p95 and p99 values are the 95th and 99th percentile values (a request latency p99 value of 500ms means that 99 out of 100 requests took 500ms or less to complete).

 If you see NaN (“not a number”) reported as a value, that means not enough responses have been received to calculate the percentile.

- Codes is the count of HTTP response codes.