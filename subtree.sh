#!/bin/bash

function subtree-repo() {
    git remote show meanjs_remote
    if [ $? -eq 0 ]; then
        echo ">>> REMOTE exists."
        git checkout meanjs_branch
        git pull --no-tags
    else
        # git remote add -f --no-tags meanjs_remote https://github.com/meanjs/mean.git
        git remote add meanjs_remote https://github.com/meanjs/mean.git
        git fetch meanjs_remote --no-tags
        git checkout -b meanjs_branch meanjs_remote/master
    fi
    git checkout master
}

function subtree-init() {
  git read-tree --prefix=backbone/ -u meanjs_branch
}

function subtree-merge() {
  git merge --squash -s recursive -Xsubtree=backbone meanjs_branch
}

function subtree-diff() {
  git diff-tree -p meanjs_branch
}

subtree-repo
subtree-diff
if [ -d './backbone' ]; then
    subtree-merge
else    
    subtree-init
fi
