/**
 * Created by peterk on 4/25/17.
 */
'use strict';

var _ = require('lodash'),
    glob = require('glob'),
    path = require('path');

/**
 * Get files by glob patterns
 */
var getGlobbedPaths = function (globPatterns, excludes) {
    // URL paths regex
    var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

    // The output array
    var output = [];

    // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
    if (_.isArray(globPatterns)) {
        globPatterns.forEach(function (globPattern) {
            output = _.union(output, getGlobbedPaths(globPattern, excludes));
        });
    } else if (_.isString(globPatterns)) {
        if (urlRegex.test(globPatterns)) {
            output.push(globPatterns);
        } else {
            var files = glob.sync(globPatterns);
            if (excludes) {
                files = files.map(function (file) {
                    if (_.isArray(excludes)) {
                        for (var i in excludes) {
                            if (excludes.hasOwnProperty(i)) {
                                file = file.replace(excludes[i], '');
                            }
                        }
                    } else {
                        file = file.replace(excludes, '');
                    }
                    return file;
                });
            }
            output = _.union(output, files);
        }
    }

    return output;
};

var initGlobalConfigFiles = function (config, assets) {
    config.files = {};

    // Setting Globbed model files
    config.files.models = getGlobbedPaths(assets.models);

    // Setting Globbed route files
    config.files.routes = getGlobbedPaths(assets.routes);

    // Setting Globbed config files
    config.files.configs = getGlobbedPaths(assets.config);

    // Setting Globbed policies files
    config.files.policies = getGlobbedPaths(assets.policies);

    console.log(config.files);
};

function initGlobalConfig() {
    var config = {};
    var assets = {
        models: 'api/models/**/*.js',
        routes: 'api/routes/**/*.js',
        config: 'api/config/**/*.js',
        policies: 'api/policies/**/*.js'
    };

    config.remote_db = 'http://localhost:5984/my_database';
    config.local_db = 'my_database';

    config.host = '127.0.0.1';
    config.port = process.env.PORT || 3000;
    config.log = {
        format: 'dev',
            fileLogger: {
            directoryPath: process.cwd(),
                fileName: 'app.log',
                maxsize: 10485760,
                maxFiles: 2,
                json: false
        }
    }

    initGlobalConfigFiles(config, assets);

    return config;
}

module.exports = initGlobalConfig();
