/**
 * Created by peterk on 4/25/17.
 */
'use strict';

var PouchDB = require('pouchdb'),
    config = require('./config');

module.exports.init = function(cb) {
    var db = new PouchDB(config.local_db);
    db.replicate.from(config.remote_db)
        .on('complete', function (result) {
            if (result)
                console.log(">>> Sync's been canceled..." + result);
            else
                console.log(">>> Sync's been completed..." + result);

            if (cb) cb(db);
        })
        .on('error', function (err) {
            console.error("!!! Error while Syncing..." + err);

            console.log(">>> Start with offline");
            if (cb) cb(db);
        });
};
