#!/bin/bash

SCRIPT=scripts/api-tests.yml
LOGFILE=test-out.log

# Run
#artillery run ${SCRIPT}  --output ${LOGFILE}

# Debugging
# DEBUG=http artillery run ${SCRIPT}  --output ${LOGFILE} --quiet
DEBUG=http:response artillery run ${SCRIPT} --output ${LOGFILE}
# DEBUG=http:capture artillery run ${SCRIPT} --output ${LOGFILE}
# DEBUG=http,http:response,http:capture artillery run ${SCRIPT} --output ${LOGFILE}
# DEBUG=* artillery run ${SCRIPT} --output ${LOGFILE}

