/**
 * Created by peterk on 11/28/16.
 */
var localDev = {
  repo: '/home/peterk/workspace/was-container/temp/src',
  path: '/home/peterk/workspace/was-container/temp/app',
  host: {host:'127.0.0.1', port:'22'},
  user: 'peterk',
  postDeploy:[
    'npm install --development'
  ].join("&&")
};

var remoteDev = {
  repo: '~/sample-app/src',
  baseDir: '~/sample-app/app',
  host: {host:'10.0.2.2', port:'10022'},
  user: 'peterk'
};

var repoUrl = '~/sample-app/src';
var DEV_SERVER = [{host:'10.0.2.2', port:'10022'}];
var PROD_SERVER = [{host:'10.0.2.2', port:'10022'}];

var WEB_PORT = "3000";
var SVC_USER = 'peterk';
var BASE_DIR = '~/sample-app';

var dev_pre_setup = [
  "mkdir -p " + BASE_DIR + "/log/nginx",
  "mkdir -p " + BASE_DIR + "/log/pm2"
];
var prod_post_deploy = [
];
var dev_post_deploy = [
];

module.exports = {
  apps : [{
    "name"      : "API",
    "script"    : "server.js",
    "instances" : 1,
    "exec_mode" : "cluster",
    "watch"     : false,
    "error_file": "/var/log/pm2/err.log",
    "out_file"  : "/var/log/pm2/out.log",
    "merge_logs": true,
    "env": {
      "NODE_ENV": "development"
    },
    "env_production" : {
      "NODE_ENV": "production"
    }
  }],
  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   * ln -sfn $PWD/../current ../../current &&
   * ln -sfn $PWD/current ../current &&
   */
  deploy : {
    production : {
      user : SVC_USER,
      host : PROD_SERVER,
      ref  : "origin/master",
      repo : repoUrl,
      path : BASE_DIR + "/app/production",
      "pre-setup": "mkdir -p " + BASE_DIR + "/log/nginx",
      "post-deploy" : prod_post_deploy.join(" && "),
      env  : {
        NODE_ENV: "production"
      }
    },
    development : {
      user : SVC_USER,
      host : DEV_SERVER,
      ref  : "origin/master",
      repo : repoUrl,
      path : BASE_DIR + "/app/development",
      "pre-deploy-local" : "echo 'This is a local executed command'",
      "pre-setup": "mkdir -p " + BASE_DIR + "/log/nginx",
      "post-deploy" : dev_post_deploy.join(" && "),
      env  : {
        NODE_ENV: "development"
      }
    },
    localDev : {
      user : localDev.user,
      host : localDev.host,
      ref  : "origin/master",
      repo : localDev.repo,
      path : localDev.path,
      "pre-deploy-local" : "echo 'This is a local executed command'",
      "post-deploy" : localDev.postDeploy,
      env  : {
        NODE_ENV: "development",
        DB_1_PORT_27017_TCP_ADDR: ""
      }
    }
  }
};
