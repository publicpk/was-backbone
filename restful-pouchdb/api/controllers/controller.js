/**
 * Created by peterk on 4/25/17.
 */

'use strict';

var PouchDB = require('pouchdb'),
    config = require('../../config/config'),
    db;

PouchDB.plugin(require('pouchdb-find'));
db = new PouchDB(config.local_db);

exports.list_users = function(req, res) {
    db.find({selector: { '_id': { '$gt': null } }}).then(function(docs) {
        res.json(docs);
    }).catch(function(err) {
        res.send(err);
    });
};

exports.create_user = function(req, res) {
    var postDoc = req.body;

    db.post(postDoc).then(function(doc) {
        res.json(doc);
    }).catch(function(err) {
        res.send(err);
    });
};

exports.read_user = function(req, res) {
    db.get(req.params.userId).then(function(doc) {
        res.json(doc);
    }).catch(function(err) {
        res.send(err);
    });
};

exports.update_user = function(req, res) {
    db.put(req.body).then(function (doc) {
        res.json(doc);
    }).catch(function (err) {
        res.send(err);
    });
};

function del_user(res, id, rev) {
    db.remove(id, rev).then(function(doc) {
        res.json(doc);
    }).catch(function(err) {
        res.send(err);
    })
}

exports.delete_user = function(req, res) {
    if ( req.params.rev ) {
        del_user(res, req.params.userId, req.params.rev);
    }
    else {
        db.get(req.params.userId).then(function(doc) {
            del_user(res, doc._id, doc._rev);
        }).catch(function(err) {
            res.send(err);
        });
    }
};
