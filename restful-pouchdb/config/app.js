/**
 * Created by peterk on 4/25/17.
 */

var express = require('./express'),
    pouch = require('./pouch'),
    config = require('./config');

module.exports.init = function(cb) {
    pouch.init(function(db) {
        var app = express.init(db, config);
        if ( cb ) cb(app, db, config);
    });
};

module.exports.start = function(cb) {
    var thiz = this;
    thiz.init(function(app, db, config) {
        app.listen(config.port, config.host, function() {
            var server = (process.env.NODE_ENV === 'secure' ? 'https://' : 'http://') + config.host + ':' + config.port;
            if ( cb ) cb(app, db, config);
        });

    })
};
