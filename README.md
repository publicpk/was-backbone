# WAS Backbone


## Init Repository
- See https://git-scm.com/book/en/v2/Git-Tools-Advanced-Merging
```
# add remote
git remote add meanjs_remote https://github.com/meanjs/mean.git
git fetch meanjs_remote --no-tags
# check it out
git checkout -b meanjs_branch meanjs_remote/master
#
git checkout master
# pull meanjs into the subdirectory of the project
git read-tree --prefix=backbone/ -u meanjs_branch
```

## Apply submodule's changes
```
# fetch submodule's changes
git checkout meanjs_branch
git pull
# merge those changes
git checkout master
git merge --squash -s recursive -Xsubtree=backbone meanjs_branch
```

## Show diff between codes in the subdirectory and the branch
```
# diff with submodule's branch
git diff-tree -p HEAD:backbone/ meanjs_branch
# diff with remote/master that the last time you fetched
git diff-tree -p HEAD:backbone/ meanjs_remote/master
```
