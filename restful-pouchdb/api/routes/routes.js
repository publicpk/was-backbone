'use strict';
var ctrl = require('../controllers/controller');

module.exports = function(app) {
    // Routes
    app.route('/api/users')
        .get(ctrl.list_users)
        .post(ctrl.create_user);

    app.route('/api/users/:userId')
        .get(ctrl.read_user)
        .put(ctrl.update_user)
        .delete(ctrl.delete_user);
    
    app.route('/api/users/:userId/:rev')
        .delete(ctrl.delete_user);

};
